FROM python:3.8

ENV TZ="Asia/Jakarta"

RUN curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add
RUN echo "deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
RUN apt-get -y update
RUN apt-get -y install google-chrome-stable
RUN wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip
RUN mv chromedriver /usr/bin/chromedriver
RUN chown root:root /usr/bin/chromedriver
RUN chmod +x /usr/bin/chromedriver

ADD requirements.txt /
RUN pip install -r requirements.txt
ADD main.py /
ADD templates /templates
ADD static /static
ADD eosl.db /
ADD site.db /
ADD wsgi.py /
CMD ["python", "main.py"]
