import pymysql

db = pymysql.connect("localhost","root","example","eosl" )


cursor = db.cursor()

#create table
# cursor.execute("CREATE TABLE hardware_eosl (device_pid TEXT, expired_date TEXT)")

#insert data
# sql = "INSERT INTO hardware_eosl (device_pid, expired_date) VALUES (%s, %s)"
# val = ("John", "Highway 21")
# cursor.execute(sql, val)

#select data
# cursor.execute("SELECT * FROM hardware_eosl")
# myresult = cursor.fetchall()
# for x in myresult:
#   print(x)

#delete data
# sql = "DELETE FROM hardware_eosl WHERE expired_date = 'Highway 21'"
# cursor.execute(sql)

#update data
# sql = "UPDATE hardware_eosl SET expired_date = 'sss 21' WHERE expired_date = 'Highway 21'"
# cursor.execute(sql)


db.commit()
db.close()