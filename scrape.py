import time
import re
import sqlite3
from selenium import webdriver
from bs4 import BeautifulSoup

urls = [
    'https://www.curvature.com/end-of-service-life-cisco-2000-2010/',
    'https://www.curvature.com/end-of-service-life-cisco-2011-2015/',
    'https://www.curvature.com/end-of-service-life-cisco-2016-2019/',
    'https://www.curvature.com/end-of-service-life-cisco/',
    'https://www.curvature.com/end-of-service-life-cisco-2021-2025/'
]

content_device_list_all = []
content_date_list_all = []

def webscrape(url):
    content_device_list = []
    content_date_list = []
    content_checker = []
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    browser=webdriver.Chrome(options=options)
    browser.get(url)
    time.sleep(2)
    try:
        e = browser.find_element_by_id('ccc-notify-accept')
        e.click()
    except:
        pass
    time.sleep(2)
    content_page = BeautifulSoup(browser.page_source, 'html.parser')
    content_page_regex = re.findall('<a.*class="paginate_button current".*>(\d+)<\/a>',str(content_page))
    content_page_regex = int(content_page_regex[0])
    for page in range(content_page_regex):
        try:
            content_html = BeautifulSoup(browser.page_source, 'html.parser')
            content_device = re.findall('<td class="sort.*">(.*)<\/td>',str(content_html))
            if content_device == content_checker:
                break
            for device in content_device:
                content_device_list.append(device)
            content_date = re.findall('<td>(\d+-\d+-\d+)<\/td>',str(content_html))
            for date in content_date:
                content_date_list.append(date)
            content_checker = content_device
            e = browser.find_element_by_class_name('next')
            e.click()
        except:
            pass

    #exit browser
    browser.close()
    return content_device_list, content_date_list

for url in urls:
    content_device_list, content_date_list = webscrape(url)
    print(content_device_list)
    for device in content_device_list:
        content_device_list_all.append(device)
    print(content_date_list)
    for date in content_date_list:
        content_date_list_all.append(date)

write = open('culvature.txt','w')
for enum, data in enumerate(content_device_list_all):
    write.write(data+' | '+content_date_list_all[enum]+'\n')
#CRUD database
try:
    db = sqlite3.connect('eosl.db')
    cursor = db.cursor()
    cursor.execute('''DROP TABLE eosl''')
    db.commit()
    db.close()
except:
    pass

try:
    db = sqlite3.connect('eosl.db')
    cursor = db.cursor()
    cursor.execute('''
        CREATE TABLE eosl(id INTEGER PRIMARY KEY, device TEXT, date TEXT)                    
    ''')
    db.close()
except:
    pass

db = sqlite3.connect('eosl.db')
cursor = db.cursor()
try:
    for enum, device in enumerate(content_device_list_all):
        cursor.execute('''INSERT INTO eosl(device, date)
                VALUES(?,?)''', (device, content_date_list_all[enum],))
except NameError:
    raise
db.commit()             
db.close()

print('Finished')