import time
import re
import sqlite3
import flask
from flask import request, jsonify, render_template, redirect, flash
from flask_login import (login_user, current_user, logout_user,
                         login_required, LoginManager, UserMixin)
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length, EqualTo, ValidationError
from selenium import webdriver
from bs4 import BeautifulSoup

app = flask.Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
app.config['SECRET_KEY'] = 'uS1NvxLuCwD98eDRyc-l7w'
#app.config["DEBUG"] = True
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'
# DATABASE
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)

    def __repr__(self):
        return f"User('{self.username}')"

class RegistrationForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired(), Length(min=2, max=20)])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError(
                'That username is taken. Please choose a different one.')

class LoginForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

#home
@app.route("/")
@login_required
def home():
	return render_template('home.html')

#register
@app.route("/register_ludes", methods=['GET', 'POST'])
@login_required
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        user = User(username=form.username.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect('/login')
    return render_template('register.html', title='Register', form=form)

@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect('/')
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect('/')
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route("/login_failed")
def login_failed():
	return render_template('login_failed.html')

@app.route("/logout")
def logout():
    logout_user()
    return redirect('/')

#api for all data
@app.route('/api/v1/resources/eosl/all', methods=['GET'])
def api_all():
    conn = sqlite3.connect('eosl.db')
    conn.row_factory = dict_factory
    cur = conn.cursor()
    all_eosl = cur.execute('SELECT * FROM eosl;').fetchall()

    return jsonify(all_eosl)

#error output
@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

#http://127.0.0.1:5000/api/v1/resources/eosl?id=3
#api for specific data
@app.route('/api/v1/resources/eosl', methods=['GET'])
def api_filter():
    query_parameters = request.args

    id = query_parameters.get('id')
    device = query_parameters.get('device')
    date = query_parameters.get('date')

    query = "SELECT * FROM eosl WHERE"
    to_filter = []

    if id:
        query += ' id=? AND'
        to_filter.append(id)
    if device:
        query += ' device=? AND'
        to_filter.append(device)
    if date:
        query += ' date=? AND'
        to_filter.append(date)
    if not (id or device or date):
        return page_not_found(404)

    query = query[:-4] + ';'

    conn = sqlite3.connect('eosl.db')
    conn.row_factory = dict_factory
    cur = conn.cursor()

    results = cur.execute(query, to_filter).fetchall()

    return jsonify(results)

#scrape page
@app.route('/scrape_page')
@login_required
def scrape_page():
    return render_template('update_scrape.html')

#scraping curvature web
@app.route('/execute_scrape')
@login_required
def execute_scrape():
    urls = [
    'https://www.curvature.com/end-of-service-life-cisco-2000-2010/',
    'https://www.curvature.com/end-of-service-life-cisco-2011-2015/',
    'https://www.curvature.com/end-of-service-life-cisco-2016-2019/',
    'https://www.curvature.com/end-of-service-life-cisco/',
    'https://www.curvature.com/end-of-service-life-cisco-2021-2025/'
    ]

    content_device_list_all = []
    content_date_list_all = []

    for url in urls:
        content_device_list, content_date_list = webscrape(url)
        print(content_device_list)
        for device in content_device_list:
            content_device_list_all.append(device)
        print(content_date_list)
        for date in content_date_list:
            content_date_list_all.append(date)

    #CRUD database
    try:
        db = sqlite3.connect('eosl.db')
        cursor = db.cursor()
        cursor.execute('''DROP TABLE eosl''')
        db.commit()
        db.close()
    except:
        pass

    try:
        db = sqlite3.connect('eosl.db')
        cursor = db.cursor()
        cursor.execute('''
            CREATE TABLE eosl(id INTEGER PRIMARY KEY, device TEXT, date TEXT)                    
        ''')
        db.close()
    except:
        pass

    db = sqlite3.connect('eosl.db')
    cursor = db.cursor()
    try:
        for enum, device in enumerate(content_device_list_all):
            cursor.execute('''INSERT INTO eosl(device, date)
                    VALUES(?,?)''', (device, content_date_list_all[enum],))
    except NameError:
        raise
    db.commit()             
    db.close()

    print('Finished')
    return redirect('/execute_scrape')

#function for scraping
def webscrape(url):
    content_device_list = []
    content_date_list = []
    content_checker = []
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    browser=webdriver.Chrome(options=options)
    browser.get(url)
    time.sleep(2)
    try:
        e = browser.find_element_by_id('ccc-notify-accept')
        e.click()
    except:
        pass
    time.sleep(2)
    content_page = BeautifulSoup(browser.page_source, 'html.parser')
    content_page_regex = re.findall('<a.*class="paginate_button current".*>(\d+)<\/a>',str(content_page))
    content_page_regex = int(content_page_regex[0])
    for page in range(content_page_regex):
        try:
            content_html = BeautifulSoup(browser.page_source, 'html.parser')
            content_device = re.findall('<td class="sort.*">(.*)<\/td>',str(content_html))
            if content_device == content_checker:
                break
            for device in content_device:
                content_device_list.append(device)
            content_date = re.findall('<td>(\d+-\d+-\d+)<\/td>',str(content_html))
            for date in content_date:
                content_date_list.append(date)
            content_checker = content_device
            e = browser.find_element_by_class_name('next')
            e.click()
        except:
            pass

    #exit browser
    browser.close()
    return content_device_list, content_date_list

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port='5003')