from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///eosl.db'
db = SQLAlchemy(app)


class Eosl(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    device = db.Column(db.String(30), unique=True, nullable=False)
    date = db.Column(db.String(30), unique=True, nullable=False)

    def __repr__(self):
        return '<Eosl %r>' % self.device