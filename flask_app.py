import flask
from flask import request, jsonify
from flask_cors import CORS, cross_origin
import pymysql

app = flask.Flask(__name__)
cors = CORS(app)
app.config["DEBUG"] = True




@app.route('/', methods=['GET'])
def home():
    return '''<h1>NetOprMgr EOSL Backend</h1>
<p>API for EOSL</p>'''

@app.route('/api/v1/eosl/hardware/insert', methods=['GET','POST'])
def hardware_insert():
    vue_submit = request.get_json()
    device_id = vue_submit['newDevice']['device_pid']
    expired = vue_submit['newDevice']['expired']
    # device_id = request.args['device_id']
    # print(device_id)
    db = pymysql.connect("localhost","root","example","eosl" )
    cursor = db.cursor()
    sql = "INSERT INTO hardware_eosl (device_pid, expired_date) VALUES (%s, %s)"
    val = (device_id, expired)
    cursor.execute(sql, val)
    db.commit()
    db.close()
    return 'insert db'

@app.route('/api/v1/eosl/hardware/read', methods=['GET'])
def hardware_read():
    db = pymysql.connect("localhost","root","example","eosl" )
    cursor = db.cursor()
    cursor.execute("SELECT * FROM hardware_eosl")
    myresult = cursor.fetchall()
    list_result = []
    for x in myresult:
      list_result.append(
          {
              "device_pid": x[0],
              "expired": x[1]
          }
      )
    db.close()
    return jsonify(list_result)


if __name__ == "__main__":
    app.run(debug=True)